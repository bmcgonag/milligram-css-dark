> A minimalist CSS framework forked from https://milligram.io, and given a dark theme, plus a few more properties / components.

## Why it's awesome

Milligram provides a minimal setup of styles for a fast and clean starting point. Just it! **Only 2kb gzipped!** It's not about a UI framework. Specially designed for better performance and higher productivity with fewer properties to reset resulting in cleaner code. Hope you enjoy!



## License

Originally Designed with ♥ by [CJ Patoilo](https://twitter.com/cjpatoilo). Licensed under the [MIT License].
Dark Theme modified by, and designed by [Brian McGonagill](https://youtube.com/c/AwesomeOpenSource)

## To Do
Make a demo page to show the compoenents and hwo they work. 

### Added Components
- Cards
- Modals
- Tooltips
- Tabs
- Sidenav
- Nav
- NavDropdown
- Container
- Custom Checkbox
- Updated Buttons
- Clickable class for Mouse Pointer hand.